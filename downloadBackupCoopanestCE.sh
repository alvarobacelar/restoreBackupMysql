#!/bin/bash

# SCRIPT DE BACKUP COOPANESTCE
if [ ! -e $1 ]; then
   NOW=$1
else
   NOW="$(date +"%Y%m%d")"
fi

AWS="$(which aws)"
YEAR="$(date +"%Y")"
MONTH="$(date +"%m")"
BACKUPDIR="/var/backupCoopanest"
FILELOG="/var/backupCoopanest/restoreBanco.log"

echo "$(date) - Iniciando o download do arquivo tar.gz para restore do banco COOPANESTCE" 
$AWS s3 cp s3://coopanest-ce/$YEAR/$MONTH/Backup-coopanest-$NOW-0000.tar.gz $BACKUPDIR/coopanestce >> $FILELOG 
if [ $? -ne 0 ]; then
    echo "$(date) - Erro ao descompactar arquivo" 
    exit 1
else
echo "$(date) - Finalizando o download do arquivo de backup do banco" 
fi
echo "$(date) - Chamando o arquivo de restore para restauração do banco do COOPANESTCE" 
sh /var/restoreBackupMysql/restoreBanco.sh coopanestce

