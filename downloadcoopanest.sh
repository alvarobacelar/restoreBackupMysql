#!/bin/bash

# SCRIPT DE BACKUP COOPANESTCE
NOW="$(date +"%Y%m%d")"
AWS="$(which aws)"
BACKUPDIR="/backup"
FILELOG="/backup/restoreBanco.log"

echo "$(date) - Iniciando o download do arquivo tar.gz para restore do banco COOPANESTCE" >> $FILELOG
$AWS s3 cp s3://coopanest-ce/2016/Backup-coopanest-$NOW-0000.tar.gz /backup/ >> $FILELOG
echo "$(date) - Finalizando o download do arquivo de backup do banco" >> $FILELOG



