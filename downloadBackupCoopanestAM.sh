#!/bin/bash

# SCRIPT DE BACKUP COOPANESTAM
if [ ! -e $1 ]; then
   NOW=$1
else
   NOW="$(date +"%Y%m%d")"
fi
YEAR="$(date +"%Y")"
#NOW="20160928"
MONTH="$(date +"%m")"
AWS="$(which aws)"
BACKUPDIR="/var/backupCoopanest"
FILELOG="/var/backupCoopanest/restoreBanco.log"

echo "$(date) - Iniciando o download do arquivo tar.gz para restore do banco" 
$AWS s3 cp s3://coopanest-am/$YEAR/$MONTH/Backup-coopanestam-$NOW-2200.tar.gz $BACKUPDIR/coopanestam >> $FILELOG 
if [ $? -ne 0 ]; then
    echo "$(date) - Erro ao baixar arquivo" 
    exit 1
else
echo "$(date) - Finalizando o download do arquivo de backup do banco" 
fi

echo "$(date) - Chamando o arquivo de restore para restauração do banco do COOPANESTAM"

sh /var/restoreBackupMysql/restoreBanco.sh coopanestam



