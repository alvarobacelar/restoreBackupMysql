#!/bin/bash

# Arquivo: retoreBanco
# Data de criação: 20.05.2016
# Autor; Álvaro Bacelar
# Email: alvaro@infoway-pi.com.br

#=== SETANDO AS VARIÁVEIS ===#
IPHOST="localhost"
MYSQL="$(which mysql)"
BD="$1"
NOW="$2"
USUARIO="root"
BACKUPDIR="/var/backupCoopanest"
FILELOG="/var/backupCoopanest/restoreBanco.log"
TAR="$(which tar)"
AWS="$(which aws)"
YEAR="$(date +"%Y")"
MONTH="$(date +"%m")"
BACKUPDIR="/var/backupCoopanest"
FILELOG="/var/backupCoopanest/restoreBanco.log"

if [ ! -e $2 ]; then
	 NOW=$2
else
	 NOW="$(date +"%Y%m%d")"
fi

start(){
	read $BD
	case $BD in
		coopanestce) downloadCoopanestCE ;;
		santacoop) downloadSantacoop ;;
		coopanestam) downloadCoopanestAM ;;
		apresentacaoce) downloadApresentacaoCE ;;
	esac
}

downloadApresentacaoCE(){
	echo "$(date) - Iniciando o download do arquivo tar.gz para restore do banco $BD"
	if [ ! -e "$BACKUPDIR"/"$BD" ]; then
  	mkdir -p $BACKUPDIR/$BD
	fi
	$AWS s3 cp s3://coopanest-ce/$YEAR/$MONTH/Backup-coopanest-$NOW-0000.tar.gz $BACKUPDIR/$BD >> $FILELOG
	if [ $? -ne 0 ]; then
	    echo "$(date) - Erro ao descompactar arquivo"
	    exit 1
	else
	echo "$(date) - Finalizando o download do arquivo de backup do banco"
	fi
	echo "$(date) - Chamando o arquivo de restore para restauração do banco do $BD"
	restoreExec
}

downloadSantacoop(){
	echo "$(date) - Iniciando o download do arquivo tar.gz para restore do banco $BD"
	if [ ! -e "$BACKUPDIR"/"$BD" ]; then
  	mkdir -p $BACKUPDIR/$BD
	fi
	$AWS s3 cp s3://santacoop/$YEAR/$MONTH/Backup-santacoopmcz-$NOW-1900.tar.gz $BACKUPDIR/$BD >> $FILELOG
	if [ $? -ne 0 ]; then
	    echo "$(date) - Erro ao descompactar arquivo"
	    exit 1
	else
	echo "$(date) - Finalizando o download do arquivo de backup do banco"
	fi
	echo "$(date) - Chamando o arquivo de restore para restauração do banco do $BD"
	restoreExec
}

downloadCoopanestAM(){
	echo "$(date) - Iniciando o download do arquivo tar.gz para restore do banco $BD"
	if [ ! -e "$BACKUPDIR"/"$BD" ]; then
  	mkdir -p $BACKUPDIR/$BD
	fi
	$AWS s3 cp s3://coopanest-am/$YEAR/$MONTH/Backup-coopanestam-$NOW-2200.tar.gz $BACKUPDIR/$BD >> $FILELOG
	if [ $? -ne 0 ]; then
	    echo "$(date) - Erro ao descompactar arquivo"
	    exit 1
	else
	echo "$(date) - Finalizando o download do arquivo de backup do banco"
	fi
	echo "$(date) - Chamando o arquivo de restore para restauração do banco do $BD"
	restoreExec
}

downloadApresentacaoCE(){
	echo "$(date) - Iniciando o download do arquivo tar.gz para restore do banco $BD"
	if [ ! -e "$BACKUPDIR"/"$BD" ]; then
  	mkdir -p $BACKUPDIR/$BD
	fi
	$AWS s3 cp s3://coopanest-ce/$YEAR/$MONTH/Backup-coopanest-$NOW-0000.tar.gz $BACKUPDIR/$BD >> $FILELOG
	if [ $? -ne 0 ]; then
	    echo "$(date) - Erro ao descompactar arquivo"
	    exit 1
	else
	echo "$(date) - Finalizando o download do arquivo de backup do banco"
	fi
	echo "$(date) - Chamando o arquivo de restore para restauração do banco do $BD"
	restoreExec
}

restoreExec() {

	echo "[$BD] $(date) - Iniciando o restore do banco $BD"
	echo "[$BD] $(date) - Verificando se existe arquivo *.tar.gz pasta $BACKUPDIR/$BD"
	NOMEBACKUPTAR=`ls -l $BACKUPDIR/$BD | awk '{print $9}'`

	#=== VERIFICANDO SE EXISTE ARQUIVO PARA REALIZAÇÃO DO RESTORE ===#
	if [ ! -s "$BACKUPDIR"/"$BD"/"$NOMEBACKUPTAR" ]; then
	    echo "[$BD] $(date) - Arquivo $BD encontrado"
	    echo "[$BD] $(date) - Entrando no diretório do arquuivo: $BACKUPDIR/$BD"
	    cd $BACKUPDIR/$BD
	    echo "[$BD] $(date) - Descompactando o arquivo $NOMEBACKUPTAR"
	    ARQUIVOBKP=`$TAR -zxvf $NOMEBACKUPTAR | tail -1`
	    if [ $? -ne 0 ]; then
	       echo "$(date) - Erro ao descompactar arquivo"
	       exit 1
	    fi
	    echo "[$BD] $(date) - Removendo arquivo $NOMETARBACKUP"
	    rm -rvf $NOMEBACKUPTAR
	    echo "[$BD] $(date) - Dropando o banco $BD existente"
	    $MYSQL -u $USUARIO -h $IPHOST --password="root!@#1nf0" -e "DROP DATABASE $BD"
	    echo "[$BD] $(date) - Criando um novo banco $BD"
	    $MYSQL -u $USUARIO -h $IPHOST --password="root!@#1nf0" -e "CREATE DATABASE $BD"
	    echo "[$BD] $(date) - Realizando o restore do arquivo $ARQUIVOBKP"
	    $MYSQL -u $USUARIO -h $IPHOST --password="root!@#1nf0" $BD < $ARQUIVOBKP
	    echo "[$BD] $(date) - Fim de restore do banco $BD"
	    echo "[$BD] $(date) - Removendo arquivo $ARQUIVOBKP"
	    rm -rfv $BACKUPDIR/$BD/*
	    echo "[$BD] $(date) - Arquivo removido"
	    echo "[$BD] $(date) - Restore do banco $BD concluído!"
	else
		echo "[ERRO] $(date) - Não foi possível realizar restore do baco $BD, não existe arquivo para realização do restore"
		exit 1
	fi

}

inicio() {

	if [ -z $BD ]; then
		echo "[ERRO] $(date) - Não foi possível iniciar o restore, não foi passado o banco no qual deseja realizar o restore"
		exit 1
	else
		start
	fi
}

inicio
