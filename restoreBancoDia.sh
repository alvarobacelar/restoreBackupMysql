#!/bin/bash

# Arquivo: retoreBanco
# Data de criação: 20.05.2016
# Autor; Álvaro Bacelar
# Email: alvaro@infoway-pi.com.br

#=== SETANDO AS VARIÁVEIS ===#
IPHOST="localhost"
MYSQL="$(which mysql)"
BD="$1"
USUARIO="root"
BACKUPDIR="/var/backupCoopanest"
FILELOG="/var/backupCoopanest/restoreBanco.log"
TAR="$(which tar)"

#==== DADOS PARA ENVIO DO ARQUIVO DE BACKUP PARA O CICLOPS ====#
#IPCICLOPS="10.0.0.22"
#USERCICLOPS="root"
#PATHARQUIVO="/var/www/html/ciclops/backups/Dolphin"

restoreExec() {

	echo "[$BD] $(date) - Iniciando o restore do banco $BD"

	echo "[$BD] $(date) - Verificando se existe arquivo *.tar.gz pasta $BACKUPDIR/$BD"
	NOMEBACKUPTAR=`ls -l $BACKUPDIR/$BD | awk '{print $9}'`

	# rm -rfv $BACKUPDIR/$BD/bkpAtual/*

	#=== VERIFICANDO SE EXISTE ARQUIVO PARA REALIZAÇÃO DO RESTORE ===#
	if [ ! -s "$BACKUPDIR"/"$BD"/"$NOMEBACKUPTAR" ]; then

	    echo "[$BD] $(date) - Arquivo $BD encontrado"

	    echo "[$BD] $(date) - Entrando no diretório do arquuivo: $BACKUPDIR/$BD"
	    cd $BACKUPDIR/$BD

  	    #echo "[$BD] $(date) - Enviando arquivo de backup para o servidor do Ciclops"
	    #scp $NOMEBACKUPTAR $USERCICLOPS@$IPCICLOPS:$PATHARQUIVO
   	    #echo "[$BD] $(date) - Arquivo enviado para o servidor do Ciclops"

	    echo "[$BD] $(date) - Descompactando o arquivo $NOMEBACKUPTAR"
	    ARQUIVOBKP=`$TAR -zxvf $NOMEBACKUPTAR | tail -1`

	    if [ $? -ne 0 ]; then
	       echo "$(date) - Erro ao descompactar arquivo"
	       exit 1
	    fi

	    echo "[$BD] $(date) - Removendo arquivo $NOMETARBACKUP"
	    rm -rvf $NOMEBACKUPTAR

	    echo "[$BD] $(date) - Dropando o banco $BD existente"
	    $MYSQL -u $USUARIO -h $IPHOST --password="root!@#1nf0" -e "DROP DATABASE $BD"

	    echo "[$BD] $(date) - Criando um novo banco $BD"
	    $MYSQL -u $USUARIO -h $IPHOST --password="root!@#1nf0" -e "CREATE DATABASE $BD"

	    echo "[$BD] $(date) - Realizando o restore do arquivo $ARQUIVOBKP"
	    $MYSQL -u $USUARIO -h $IPHOST --password="root!@#1nf0" $BD < $ARQUIVOBKP
	    echo "[$BD] $(date) - Fim de restore do banco $BD"

	    echo "[$BD] $(date) - Removendo arquivo $ARQUIVOBKP"
	    #mv $BACKUPDIR/$BD/*.tar.gz $BACKUPDIR/$BD/bkpAtual/
	    rm -rfv $BACKUPDIR/$BD/*
	    echo "[$BD] $(date) - Arquivo removido"

	    echo "[$BD] $(date) - Restore do banco $BD concluído!"

	else

		echo "[ERRO] $(date) - Não foi possível realizar restore do baco $BD, não existe arquivo para realização do restore"
		exit 1
	fi

}

inicio() {

	if [ -z $BD ]; then
		echo "[ERRO] $(date) - Não foi possível iniciar o restore, não foi passado o banco no qual deseja realizar o restore"
		exit 1
	else
		restoreExec
	fi
}

inicio
