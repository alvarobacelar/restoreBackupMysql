#!/bin/bash

# SCRIPT DE BACKUP SANTACOOP
if [ ! -e $1 ]; then
   NOW=$1
else
   NOW="$(date +"%Y%m%d")"
fi
YEAR="$(date +"%Y")"
#NOW="20160928"
AWS="$(which aws)"
MONTH="$(date +"%m")"
BACKUPDIR="/var/backupCoopanest"
FILELOG="/var/backup/restoreBanco.log"

echo "$(date) - Iniciando o download do arquivo tar.gz para restore do banco SANTACOOP"
$AWS s3 cp s3://santacoop/$YEAR/$MONTH/Backup-santacoopmcz-$NOW-1900.tar.gz $BACKUPDIR/santacoop >> $FILELOG 

if [ $? -ne 0 ]; then
    echo "$(date) - Erro ao descompactar arquivo" 
    exit 1
else 
echo "$(date) - Finalizando o download do arquivo de backup do banco" 
fi 

echo "$(date) - Chamando o arquivo de restore para restauração do banco do SANTACOOP"

sh /var/restoreBackupMysql/restoreBanco.sh santacoop
