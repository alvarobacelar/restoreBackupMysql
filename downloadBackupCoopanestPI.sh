#!/bin/bash

# SCRIPT DE BACKUP COOPANESTPI
NOW="$(date +"%Y%m%d")"
AWS="$(which aws)"
BACKUPDIR="/backup"
FILELOG="/backup/restoreBanco.log"

echo "$(date) - Iniciando o download do arquivo tar.gz para restore do banco COOPANESTPI" >> $FILELOG
$AWS s3 cp s3://coopanest-pi/2016/Backup-coopanestpi-$NOW-1700.tar.gz $BACKUPDIR/coopanestpi >> $FILELOG
echo "$(date) - Finalizando o download do arquivo de backup do banco" >> $FILELOG

echo "$(date) - Chamando o arquivo de restore para restauração do banco do COOPANESTPI" >> $FILELOG

sh $BACKUPDIR/restoreBancoCoopanest/restoreBanco.sh coopanestpi
